﻿using System;
using System.Globalization;
using System.Reflection;
using System.Runtime.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace XxeDotNet45
{
    [TestClass]
    public class UnitTest1
    {
        static bool HasAttribute(MemberInfo memberInfo, Type type)
        {
            return memberInfo.GetCustomAttributes(type, false).Length > 0;
        }

        [TestMethod]
        public void TestMethod1()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            foreach (Assembly assembly in assemblies)
            {
                foreach (Type type in assembly.GetTypes())
                {
                    if (!type.IsAbstract && !type.IsEnum && type.IsSerializable)
                    {
                        if (typeof(ISerializable).IsAssignableFrom(type))
                        {
                            Console.WriteLine("ISerializable {0}", type.FullName);
                        }
                        if (typeof(IObjectReference).IsAssignableFrom(type))
                        {
                            Console.WriteLine("IObjectReference {0}", type.FullName);
                        }
                        if (typeof(IDeserializationCallback).IsAssignableFrom(type))
                        {
                            Console.WriteLine("IDeserializationCallback {0}", type.FullName);
                        }

                        foreach (
                            MethodInfo m in type.GetMethods(BindingFlags.Public |
                                BindingFlags.NonPublic |
                                BindingFlags.Instance
                            )
                        )
                        {
                            if (HasAttribute(m, typeof(OnDeserializingAttribute)))
                            {
                                Console.WriteLine("OnDeserializing {0}", type.FullName);
                            }
                            if (HasAttribute(m, typeof(OnDeserializedAttribute)))
                            {
                                Console.WriteLine("OnDeserialized {0}", type.FullName);
                            }
                            if (m.Name == "Finalize" && m.DeclaringType != typeof(object))
                            {
                                Console.WriteLine("Finalizable {0}", type.FullName);
                            }
                        }
                    }
                }
            }
        }
    }
}
