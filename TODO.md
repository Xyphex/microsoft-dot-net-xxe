# To-do

## Showcase XXE instances

[https://www.owasp.org/index.php/XML_External_Entity_(XXE)_Prevention_Cheat_Sheet#.NET]

| XML Parser             | Safe by Default? |
| ---------------------- | ---------------- |
| LINQ to XML            | Yes              |
| XmlDictionaryReader    | Yes              |
| XmlDocument            | -                |
| ... prior to 4.5.2     | No               |
| ... in versions 4.5.2+ | Yes              |
| XmlNodeReader          | Yes              |
| XmlReader              | Yes              |
| XmlTextReader          | -                |
| ... prior to 4.5.2     | No               |
| ... in versions 4.5.2+ | Yes              |
| XPathNavigator         | -                |
| ... prior to 4.5.2     | No               |
| ... in versions 4.5.2+ | Yes              |
| XslCompiledTransform   | Yes              |
